/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.entities;

import java.io.Serializable;

/**
 *
 * @author LENOVO
 */
public class MessageContainer implements Serializable
{
    public static enum MessageType {PING, PING_RESPONSE, OPERATION, CUSTOM} ;
    
    public static final String REQUESTED_OPERATION_PARAM = "REQUESTED_OPERATION_PARAM" ;
    
    protected String m_sourcePeerId ;
    protected Serializable m_messageData ;
    protected MessageContainer.MessageType m_messageType ;
    
    public MessageContainer()
    {
        this.m_messageType = MessageContainer.MessageType.CUSTOM ;
    }

    public String getSourcePeerId() 
    {
        return m_sourcePeerId;
    }

    public Serializable getMessageData() 
    {
        return m_messageData;
    }

    public void setSourcePeerId(String i_sourcePeerId) 
    {
        this.m_sourcePeerId = i_sourcePeerId;
    }

    public void setMessageData(Serializable i_messageData) 
    {
        this.m_messageData = i_messageData;
    }

    public MessageType getMessageType() 
    {
        return this.m_messageType;
    }

    public void setMessageType(MessageType i_messageType) 
    {
        this.m_messageType = i_messageType;
    }
    
    @Override
    public String toString()
    {
        String toReturn = MessageContainer.class.getCanonicalName() ;
        toReturn += "\nm_sourcePeerId : " + ((null == this.m_sourcePeerId) ? "<null>" : this.m_sourcePeerId) ;
        toReturn += "\nm_messageType : " + ((null == this.m_messageType) ? "<null>" : this.m_messageType.name()) ;
        toReturn += "\nm_messageData : " + ((null == this.m_messageData) ? "<null>" : this.m_messageData.toString()) ;
        return toReturn ;
    }
}
