/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.backend;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.HashMap;
import java.util.Vector;
import sockettunnelling.entities.MessageContainer;
import sockettunnelling.entities.RegisterClientRequestData;
import sockettunnelling.entities.SendMessageToPeerRequestData;
import ssrcommon.SSRMutexProtectedOperation;
import ssrcommon.SSROrderedMutexGateKeeperForObject;
import ssrcommon.SSROrderedMutexProtectedHashMap;
import ssrcommon.SSROrderedReadWriteMutex;
import ssrcommon.threads.SSRBasicBackendRequest;
import ssrcommon.threads.SSRBasicBackendResponse;
import ssrcommon.threads.SSRBasicBackendServicesWorkerThread;

/**
 *
 * @author LENOVO
 */
public class SocketTunnellingBackendWorkerThread extends SSRBasicBackendServicesWorkerThread
{   
    public static final String REGISTER_CLIENT = "REGISTER_CLIENT" ;
    public static final String SEND_MESSAGE_TO_PEER = "SEND_MESSAGE_TO_PEER" ;
    public static final String PING_PEER_SYNCH = "PING_PEER_SYNCH" ;
    
    public static final String DESTINATION_PEER_NOT_FOUND = "DESTINATION_PEER_NOT_FOUND" ;
    
    public static final String PING_OK = "PING_OK" ;
    public static final String PING_FAILURE = "PING_FAILURE" ;
    
    protected SocketTunnellingBackendServer m_server ;
    
    public SocketTunnellingBackendWorkerThread(Socket i_socketToGiveServiceTo, 
            SocketTunnellingBackendServer i_serverReferenceToStore) throws Exception
    {
        super(i_socketToGiveServiceTo) ;
        this.m_server = i_serverReferenceToStore ;
    }
    
    @Override
    protected void processClientRequest() throws Exception
    {
        System.out.println("Entering SocketTunnellingBackendServer.processClientRequest") ;
        
        String clientRequestCode = ((SSRBasicBackendRequest)this.m_clientRequest).m_serviceRequestCode ;
        switch(clientRequestCode)
        {
            case SocketTunnellingBackendWorkerThread.REGISTER_CLIENT :
            {
                this.handleRegisterClient() ;
                break ;
            }
            case SocketTunnellingBackendWorkerThread.SEND_MESSAGE_TO_PEER :
            {
                this.handleSendMessageToPeer() ;
                break ;
            }
            case SocketTunnellingBackendWorkerThread.PING_PEER_SYNCH :
            {
                this.handlePingPeerSynch() ;
                break ;
            }
            default :
            {
                System.out.println("Unrecognized client request code : " + clientRequestCode) ;
                throw new Exception("Unrecognized client request code : " + clientRequestCode) ;
            }
        }
        System.out.println("Response to client : " + this.m_responseToClient.toString()) ;
        return ;
    }
    
    protected void handleRegisterClient() throws Exception
    {
        System.out.println("Entering SocketTunnellingBackendServer.handleRegisterClient") ;
        RegisterClientRequestData registerData = (RegisterClientRequestData)
                ((SSRBasicBackendRequest)this.m_clientRequest).m_requestData ;
        
        SSROrderedMutexProtectedHashMap<String, SocketTunnellingBackendServer.RegisteredClientAddress>
                clientsAddresses = this.m_server.getRegisteredClientsHashMap() ;
        SocketTunnellingBackendServer.RegisteredClientAddress oneClientAddress ;
        oneClientAddress = clientsAddresses.get(registerData.getClientId()) ;
        if(null == oneClientAddress)
        {
            oneClientAddress = this.m_server.createRegisteredClientAddressInstance() ;
            oneClientAddress.setClientId(registerData.getClientId()) ;
            
            // The following queue creation is to be done only if the client was not already registered
            this.m_server.getClientsMessageQueue().put(oneClientAddress.getClientId(), 
                    new SSROrderedMutexGateKeeperForObject<Vector<MessageContainer>>(new Vector<MessageContainer>())) ;
        }
        oneClientAddress.setClientSocket(this.m_serviceSocket) ;
        this.m_server.getRegisteredClientsHashMap().put(registerData.getClientId(), oneClientAddress);
        
        this.prepareStandardSuccessfulResponse() ;
        this.writeResponseToSocket() ;
    }
    
    protected void handleSendMessageToPeer() throws Exception
    {
        System.out.println("Entering SocketTunnellingBackendServer.handleSendMessageToPeer") ;
        SendMessageToPeerRequestData sendMessageData = (SendMessageToPeerRequestData)
                ((SSRBasicBackendRequest)this.m_clientRequest).m_requestData ;
        
        SSROrderedMutexGateKeeperForObject<Vector<MessageContainer>> messageQueue = 
                this.m_server.getClientsMessageQueue().get(sendMessageData.getDestinationPeerId()) ;
        if(null == messageQueue)
        {
            // Host destination has not been registered for receiving message. Return failure.
            this.prepareStandardFailureResponse() ;
            ((SSRBasicBackendResponse)this.m_responseToClient).m_serverResponseCode = 
                    SocketTunnellingBackendWorkerThread.DESTINATION_PEER_NOT_FOUND ;
            this.writeResponseToSocket() ;
            return ;
        }
        
        SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean> insertOperation = 
                this.m_server.getInsertMessageOperation() ;
        MessageContainer oneMessageContainer = sendMessageData.getMessageData() ;
        insertOperation.m_operationParameters = new Object[]{oneMessageContainer} ;
        messageQueue.<Boolean>performMutexProtectedOperation(insertOperation, 
                SSROrderedReadWriteMutex.ResourceOperation.WRITE) ;
        
        // Wakeup the postmaster so that it can post the messages
        this.m_server.wakeupPostMaster() ;
        
        this.prepareStandardSuccessfulResponse() ;
        this.writeResponseToSocket() ;
        return ;
    }
    
    // TODO : VERY IMPORTANT. This method has not been made thread safe right now
    //        It directly accesses the socket that was registered for the peer and
    //        this is not safe. Fix this ASAP.
    //        ** THIS FLOW IS NOT YET FUNCTIONAL **
    protected void handlePingPeerSynch() throws Exception
    {
        this.logMessage("Entering SocketTunnellingBackendWorkerThread.handlePingPeer");
        this.logMessage("THIS FLOW IS NOT YET FUNCTIONAL.") ;
        
        this.prepareStandardFailureResponse();
        this.writeResponseToSocket();
        return ;
        
        /*SendMessageToPeerRequestData pingMessageRequestData = (SendMessageToPeerRequestData)
                (((SSRBasicBackendRequest)this.m_clientRequest).m_requestData) ;
        SSROrderedMutexProtectedHashMap<String, SocketTunnellingBackendServer.RegisteredClientAddress> 
                registeredPeersHashMap = this.m_server.getRegisteredClientsHashMap() ;
        
        // If the peer is not found, send response saying the same
        if(false == registeredPeersHashMap.containsKey(pingMessageRequestData
                .getDestinationPeerId()))
        {
            this.prepareStandardFailureResponse();
            ((SSRBasicBackendResponse) this.m_responseToClient).m_serverResponseCode
                    = SocketTunnellingBackendWorkerThread.DESTINATION_PEER_NOT_FOUND;
            this.writeResponseToSocket();
            return;
        }
        
        // Get registered peer address for the destination.
        SocketTunnellingBackendServer.RegisteredClientAddress destPeerAddress = 
                registeredPeersHashMap.get(pingMessageRequestData.getDestinationPeerId()) ;
        
        // Prepare ping message container
        MessageContainer pingMessageContainer = new MessageContainer() ;
        pingMessageContainer.setMessageType(MessageContainer.MessageType.PING);
        pingMessageContainer.setMessageData(pingMessageRequestData.getSourcePeerId());
        pingMessageContainer.setSourcePeerId(pingMessageRequestData.getSourcePeerId());
        
        // Write ping request and read response
        try
        {
            // Write ping
            ObjectOutputStream pingOutputStream = new ObjectOutputStream(destPeerAddress
                    .getClientSocket().getOutputStream()) ;
            pingOutputStream.writeObject(pingMessageContainer) ;
            
            // Read ping response
            ObjectInputStream pingInputStream = new ObjectInputStream(destPeerAddress
                    .getClientSocket().getInputStream()) ;
            String pingResponse = (String)pingInputStream.readObject() ;
            
            // Prepare response to ping origin
            if(true == pingResponse.equals(SocketTunnellingBackendWorkerThread.PING_OK))
                this.prepareStandardSuccessfulResponse();
            else
                this.prepareStandardFailureResponse();
            
            ((SSRBasicBackendResponse) this.m_responseToClient).m_responseData
                    = pingResponse ;
            
            // Send response to ping origin
            this.writeResponseToSocket();
            
            return ;
        }
        catch(Exception e)
        {
            this.showExceptionError("Ping destination " 
                    + pingMessageRequestData.getDestinationPeerId() + "failed", e);
            this.prepareStandardFailureResponse();
            ((SSRBasicBackendResponse) this.m_responseToClient).m_serverResponseCode
                    = SocketTunnellingBackendWorkerThread.PING_FAILURE ;
            this.writeResponseToSocket() ;
            return ;
        }*/
    }
}
