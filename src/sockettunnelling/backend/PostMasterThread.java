/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.backend;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;
import sockettunnelling.entities.MessageContainer;
import ssrcommon.SSRMutexProtectedOperation;
import ssrcommon.SSROrderedMutexGateKeeperForObject;
import ssrcommon.SSROrderedMutexProtectedHashMap;
import ssrcommon.SSROrderedReadWriteMutex;
import ssrcommon.threads.SSRThreadForBackgroundWork;

/**
 *
 * @author LENOVO
 */
public class PostMasterThread extends SSRThreadForBackgroundWork
{
    protected Integer m_loopWaitDurationInMillis ;
    protected Boolean m_wakeupOrderIssued ;
    protected final Integer m_wakeupLockObject ;
    protected SocketTunnellingBackendServer m_server ;
    protected SSRMutexProtectedOperation<Vector<MessageContainer>, MessageContainer> 
            m_removeFirstMessageOperation ;
    protected SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean> 
            m_checkIsEmptyOperation ;
    
    public PostMasterThread(SocketTunnellingBackendServer i_ownerServer) throws Exception
    {
        this.m_server = i_ownerServer ;
        this.m_wakeupOrderIssued = false ;
        this.m_wakeupLockObject = 10 ;
        this.m_loopWaitDurationInMillis = 10000 ;
        this.cacheMessageQueueOperationObjects() ;
    }
    
    public void issueWakeupRequest() throws Exception
    {
        this.m_wakeupOrderIssued = true ;
        this.wakeupThread() ;
    }
    
    public void runMainLogic() throws Exception
    {
        SSROrderedMutexProtectedHashMap<String, SSROrderedMutexGateKeeperForObject<
            Vector<MessageContainer>>> messageQueue = this.m_server.getClientsMessageQueue() ;
        Boolean unprocessedMessages = true ;
        
        while(true == unprocessedMessages)
        {
            try
            {
                this.logMessage("Starting main loop for PostMasterThread") ;
                unprocessedMessages = false ;
                Set<String> clientIdSet = messageQueue.keySet() ;
                Iterator<String> clientIdIterator = clientIdSet.iterator() ;
                while(true == clientIdIterator.hasNext())
                {
                    String currentClientId = clientIdIterator.next() ;
                    this.logMessage("Processing  client : " + currentClientId);

                    // Get destination message socket
                    SocketTunnellingBackendServer.RegisteredClientAddress oneAddress = 
                            this.m_server.getRegisteredClientsHashMap().get(currentClientId) ;

                    // Get one message queue
                    SSROrderedMutexGateKeeperForObject<Vector<MessageContainer>> oneMessageQueue =
                            messageQueue.get(currentClientId) ;

                    // If message queue has no messages, continue
                    if(true == this.checkIfMessageQueueEmpty(oneMessageQueue))
                    {
                        this.logMessage("Destination with clientId : \"" + currentClientId 
                                + "\" has no messages pending for delivery.") ;
                        continue ;
                    }

                    // check if destination socket is open for communication or not
                    Socket destinationSocket = oneAddress.m_clientSocket ;
                    if(false == destinationSocket.isConnected())
                    {
                        unprocessedMessages = true ;
                        // TODO : Do proer error handling here. The source peer has to be 
                        //        notified of this problem
                        this.logMessage("Destination with clientId : \"" + currentClientId 
                                + "\" has closed its socket.") ;
                        continue ;
                    }

                    // Post all the messages in this queue
                    while(false == this.checkIfMessageQueueEmpty(oneMessageQueue))
                    {
                        try
                        {
                            MessageContainer oneMessageContainer ;
                            try
                            {
                                oneMessageContainer = this.removeFirstMessageFromQueue(oneMessageQueue);
                            }
                            catch (NoSuchElementException e) 
                            {
                                this.logMessage("Current message queue finished.") ;
                                break ;
                            }
                            this.logMessage("Posting message (container) :\n" 
                                    + oneMessageContainer.toString()) ;
                            ObjectOutputStream objectStreamToWriteMessage = new ObjectOutputStream(
                                    destinationSocket.getOutputStream()) ;
                            objectStreamToWriteMessage.writeObject(oneMessageContainer) ;
                        }
                        catch(Exception e)
                        {
                            this.showExceptionError("Exception caught while processing " 
                                    + "message queue of client : " + currentClientId 
                                    + "\nSkipping message queue for this iteration.", e);
                            unprocessedMessages = true ;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                this.showExceptionError("Exception in PostMasterThread in main loop.", e);
                unprocessedMessages = true ;
            }
            
            if(false == unprocessedMessages)
            {
                this.logMessage("PostMaster has no more unprocessed messages. Going to wait until woken up.") ;
                this.waitUntilWoken();
                unprocessedMessages = true ;
            }
            else
            {
                this.logMessage("PostMaster finished one loop with leftover unprocessed messages. Going to wait for : " 
                        + this.m_loopWaitDurationInMillis / 1000 + " seconds before starting net iteration.") ;
                this.waitUntilWoken(this.m_loopWaitDurationInMillis.longValue());
            }
        }
    }
    
    private void waitUntilWoken() throws Exception
    {
        this.waitUntilWoken(0l) ;
    }
    
    private void waitUntilWoken(Long i_sleepTimeoutDuration) throws Exception
    {
        synchronized(this.m_wakeupLockObject)
        {
            while(false == this.m_wakeupOrderIssued)
                this.m_wakeupLockObject.wait(i_sleepTimeoutDuration) ;
        }
        
        this.m_wakeupOrderIssued = false ;
    }
    
    private void wakeupThread() throws Exception
    {
        synchronized(this.m_wakeupLockObject)
        {
            this.m_wakeupLockObject.notifyAll();
        }
    }
    
    private void cacheMessageQueueOperationObjects() throws Exception
    {
        this.m_removeFirstMessageOperation = this.m_server.getRemoveFirstMessageOperation() ;
        this.m_checkIsEmptyOperation = this.m_server.getIsEmptyOperation() ;
    }
    
    private Boolean checkIfMessageQueueEmpty(SSROrderedMutexGateKeeperForObject<Vector<MessageContainer>> 
            i_messageQueueToCheck) throws Exception
    {
        i_messageQueueToCheck.performMutexProtectedOperation(this.m_checkIsEmptyOperation, 
                SSROrderedReadWriteMutex.ResourceOperation.READ);
        return this.m_checkIsEmptyOperation.m_operationResult ;
    }
    
    private MessageContainer removeFirstMessageFromQueue(SSROrderedMutexGateKeeperForObject<
            Vector<MessageContainer>> i_messageQueueToOperate) throws Exception
    {
        i_messageQueueToOperate.performMutexProtectedOperation(this.m_removeFirstMessageOperation, 
                SSROrderedReadWriteMutex.ResourceOperation.WRITE);
        return this.m_removeFirstMessageOperation.m_operationResult ;
    }
}
