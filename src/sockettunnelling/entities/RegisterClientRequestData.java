/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.entities;

import java.io.Serializable;
import ssrcommon.entities.ISSRRequestData;

/**
 *
 * @author LENOVO
 */
public class RegisterClientRequestData implements ISSRRequestData, Serializable
{
    protected String m_clientId ;

    public void setClientId(String i_clientId) 
    {
        this.m_clientId = i_clientId;
    }

    public String getClientId() 
    {
        return m_clientId;
    }
    
    @Override
    public Boolean vaidate() throws Exception 
    {
        return true ;
    }
}
