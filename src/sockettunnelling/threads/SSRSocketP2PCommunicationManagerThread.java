/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.threads;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import sockettunnelling.backend.SocketTunnellingBackendServer;
import sockettunnelling.backend.SocketTunnellingBackendWorkerThread;
import sockettunnelling.entities.MessageContainer;
import sockettunnelling.entities.RegisterClientRequestData;
import sockettunnelling.entities.SendMessageToPeerRequestData;
import ssrcommon.design.SSRKeyControlledCallbackGateKeeper;
import ssrcommon.exceptions.SSRBasicException;
import ssrcommon.threads.SSRBasicBackendRequest;
import ssrcommon.threads.SSRBasicBackendResponse;
import ssrcommon.threads.SSRBasicBackendServicesThread;

/**
 *
 * @author LENOVO
 */
public class SSRSocketP2PCommunicationManagerThread extends SSRBasicBackendServicesThread
{
    public static final String CLIENT_ID_FOR_P2P_PARAMETER_NAME = "CLIENT_ID_FOR_P2P_PARAMETER" ;
    public static final Integer DEFAULT_POLLING_GAP_DURATION = 5000 ;
    
    protected static SSRSocketP2PCommunicationManagerThread ms_uniqueInstance ;
    
    protected Boolean m_stopFlag ;
    protected Integer m_pollingGapDuration ;
    protected ObjectInputStream m_objectStreamForMessageFromBackend ;
    
    protected HashMap<String, Map.Entry<String, Object> > m_operationsDataHashMap ;
    
    public static SSRSocketP2PCommunicationManagerThread getUniqueInstance() throws Exception
    {
        SSRSocketP2PCommunicationManagerThread toReturn = null ;
        if(null == SSRSocketP2PCommunicationManagerThread.ms_uniqueInstance)
        {
            throw new SSRBasicException("getUniqueInstance was called for " 
                    + "SSRSocketP2PCommunicationManagerThread without setting unique instance.") ;
        }
        else
        {
            toReturn = SSRSocketP2PCommunicationManagerThread.ms_uniqueInstance ;
        }
        return toReturn ;
    }
    
    public static synchronized void setUniqueInstance(SSRSocketP2PCommunicationManagerThread 
            i_uniqueInstanceToStore, Boolean i_clobber) throws Exception
    {
        if(true == i_clobber || null == SSRSocketP2PCommunicationManagerThread.ms_uniqueInstance)
        {
            SSRSocketP2PCommunicationManagerThread.ms_uniqueInstance = i_uniqueInstanceToStore ;
        }
        else
        {
            throw new SSRBasicException("SSRSocketP2PCommunicationManagerThread.setUniqueInstance "
                    + "is being reset.") ;
        }
    }
    
    public SSRSocketP2PCommunicationManagerThread(HashMap<Object, Object> i_parametersHashMap) 
            throws Exception
    {
        this(i_parametersHashMap, SSRBasicBackendServicesThread.DEFAULT_SOCKET_CONNECTION_TIMEOUT_DURATION,
                SSRSocketP2PCommunicationManagerThread.DEFAULT_POLLING_GAP_DURATION);
    }
        
    public SSRSocketP2PCommunicationManagerThread(HashMap<Object, Object> i_parametersHashMap,
            Integer i_socketTimeoutToSet, Integer i_pollingGapToSet) 
            throws Exception
    {
        super(null, null, i_parametersHashMap, i_socketTimeoutToSet);
        this.setStopFlag(false) ;
        this.m_pollingGapDuration = i_pollingGapToSet ;
        this.m_operationsDataHashMap = new HashMap<String, Map.Entry<String, Object> >() ;
    }
    
    public void registerOperation(String i_operationName, String i_callbackIdentifier, 
            Object i_callbackKey) throws Exception
    {
        AbstractMap.SimpleEntry<String, Object> oneEntry = 
                new AbstractMap.SimpleEntry<String, Object>(i_callbackIdentifier, i_callbackKey) ;
        this.m_operationsDataHashMap.put(i_operationName, oneEntry) ;
    }

    public Boolean getStopFlag() 
    {
        return this.m_stopFlag;
    }

    public void setStopFlag(Boolean i_stopFlag) 
    {
        this.m_stopFlag = i_stopFlag ;
    }
    
    @Override
    public void runMainLogic() throws Exception
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.runMainLogic") ;
        
        if(null == this.m_socketToBackend)
            this.registerPeerWithBackendForCommunication();
        
        while(false == this.getStopFlag())
        {
            MessageContainer receivedMessageContainer = null ;
            // TODO : See what kind of exceptions are caught here, when connections are dropped,
            //        and in those cases, re-establish the connection.
            try
            {
                this.maintainConnection();
                receivedMessageContainer = (MessageContainer)this.m_objectStreamForMessageFromBackend
                        .readObject() ;
            }
            catch(Exception e)
            {
                this.logMessage("Exception caught while polling for message data :\n", e) ;
            }
            
            // Message hadling should be inside try block because otherwise, exception
            // in a single handling will bring the thread down.
            try
            {
                // Send message container for handling only if something was received
                if(null != receivedMessageContainer)
                {
                    this.handleMessage(receivedMessageContainer);
                    receivedMessageContainer = null;
                }
            }
            catch(Exception e)
            {
                this.showExceptionError("Exception caught while trying to handle received message:\n", e);
                this.handleErroredMessage(receivedMessageContainer) ;
                // At the beginning of every loop for message receiving, receivedMessageContainer
                // is made null. Hence we are not making it null here explicitly
            }
            
            // Wait until next polling iteration
            try
            {
                Thread.sleep(this.m_pollingGapDuration);
            }
            catch(InterruptedException e)
            {
                // This is not a big error. It just means the thread was interrupted for some reason
                // We will not end flow, we will just communicate this exception
                this.logMessage("Socket P2P communication manager thread received " 
                        + "InterruptedException during polling gap.", e) ;
            }
        }
    }
    
    public void registerPeerWithBackendForCommunication() throws Exception
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.registerPeerWithBackendForCommunication") ;
        
        // Create socket connection
        this.m_socketToBackend = new Socket() ;
        this.m_socketToBackend.connect(this.getInetSocketAddressForConnection(),
                this.m_socketConnectionTimeoutInMilliseconds);
        
        // Backend request object created
        SSRBasicBackendRequest registerRequest = new SSRBasicBackendRequest() ;
        this.m_requestToBackend = registerRequest ;
        registerRequest.m_requestId = "1" ;
        registerRequest.m_serviceRequestCode = SocketTunnellingBackendWorkerThread.REGISTER_CLIENT ;
        registerRequest.m_requestDataSize = new Integer(1) ;
        RegisterClientRequestData requestData = new RegisterClientRequestData() ;
        String inputParamClientId = (String)this.m_parametersHashMap.get(SSRSocketP2PCommunicationManagerThread
                .CLIENT_ID_FOR_P2P_PARAMETER_NAME) ;
        requestData.setClientId(inputParamClientId) ;
        registerRequest.m_requestData = requestData ;

        // Send request to backend server
        registerRequest.writeRequestToOutputStream(this.m_socketToBackend.getOutputStream()) ;
        
        // Receive response from backend server
        SSRBasicBackendResponse registerUserResponse = new SSRBasicBackendResponse() ;
        this.m_responseFromBackend = registerUserResponse ;
        registerUserResponse.readResponseFromInputStream(this.m_socketToBackend.getInputStream()) ;
        
        // Check response from backend server
        if(false == registerUserResponse.m_serverResponseCode.equals(SSRBasicBackendResponse.RESPONSE_OK))
        {
            this.logMessage("Peer registeration with backend for P2P communication, FAILED.") ;
            SSRBasicException toThrow = new SSRBasicException(
                    "Peer registration to backend server failed. Response : " 
                    + registerUserResponse.m_serverResponseCode) ;
            toThrow.setExceptionDetailObject(registerUserResponse) ;
            throw toThrow ;
        }

        this.logMessage("Peer successfully registered with backend for P2P communication.") ;
    }
    
    public void handleMessage(MessageContainer i_messageToHandle) throws Exception
    {
        // TODO : Put other possible means of handling message, like callback etc.
        //        if needed. Right now only a single way to handle, via implementation
        //        of the processMessage method will be done.
        if(true == this.processControlMessage(i_messageToHandle))
        {
            this.logMessage("Control message handled :\n" + i_messageToHandle.toString()) ;
            return ;
        }
        else if(true == this.processOperationMessage(i_messageToHandle))
        {
            this.logMessage("Control message handled :\n" + i_messageToHandle.toString()) ;
            return ;
        }
        this.processMessage(i_messageToHandle) ;
    }
    
    protected void handleErroredMessage(MessageContainer i_erroredMessageContainer)
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.handleErroredMessage");
        // Empty implementation. To be implemented by the application.
    }
    
    // The Boolean return value tells whether the message was consumed or not
    protected Boolean processControlMessage(MessageContainer i_messageToProcess) 
            throws Exception
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.processControlMessage") ;
        Boolean messageProcessedIndicator = false ;
        if(MessageContainer.MessageType.PING == i_messageToProcess.getMessageType())
        {
            this.logMessage("Found message type : PING") ;
            messageProcessedIndicator = true ;
            /*ObjectOutputStream streamForResponse = new ObjectOutputStream(this
                    .m_socketToBackend.getOutputStream()) ;
            streamForResponse.writeObject(SocketTunnellingBackendWorkerThread.PING_OK) ;*/

            String sourcePeerId = i_messageToProcess.getSourcePeerId() ;
            
            MessageContainer pingReplyMessageContainer = new MessageContainer() ;
            pingReplyMessageContainer.setMessageType(MessageContainer.MessageType.PING_RESPONSE);
            pingReplyMessageContainer.setSourcePeerId(sourcePeerId);
            pingReplyMessageContainer.setMessageData(i_messageToProcess.getMessageData());
            
            SendMessageToPeerRequestData pingResponseData = new SendMessageToPeerRequestData() ;
            pingResponseData.setDestinationPeerId(sourcePeerId);
            pingResponseData.setMessageData(pingReplyMessageContainer);
            // TODO : Decide whether this should be the peer id of this one or not.
            pingResponseData.setSourcePeerId(sourcePeerId);
            
            SSRSocketP2PMessagePosterThread pingReplyThread = 
                    new SSRSocketP2PMessagePosterThread(pingResponseData) ;
            pingReplyThread.start();
            // TODO: check whether thread worked properly
        }
        else if(MessageContainer.MessageType.PING_RESPONSE == i_messageToProcess.getMessageType())
        {
            this.logMessage("Found message type : PING_RESPONSE") ;
            messageProcessedIndicator = true ;
            this.handlePingResponse(i_messageToProcess) ;
        }
        return messageProcessedIndicator ;
    }
    
    // The Boolean return value tells whether the message was consumed or not
    protected Boolean processOperationMessage(MessageContainer i_messageToProcess) 
            throws Exception
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.processOperationMessage") ;
        Boolean messageProcessedIndicator = false ;
        if(MessageContainer.MessageType.OPERATION == i_messageToProcess.getMessageType())
        {
            this.logMessage("Found message type : OPERATION") ;
            messageProcessedIndicator = true ;
            HashMap<String, Serializable> operationMessageParams = (HashMap<String, Serializable>)
                    i_messageToProcess.getMessageData() ;
            String operationName = (String)operationMessageParams.get(
                    MessageContainer.REQUESTED_OPERATION_PARAM) ;
            Map.Entry<String, Object> operationEntry = this.m_operationsDataHashMap
                    .get(operationName) ;
            String callbackIdentifier = operationEntry.getKey() ;
            Object callbackKey = operationEntry.getValue() ;
            SSRKeyControlledCallbackGateKeeper.getInstance().performCallback(
                    callbackIdentifier, callbackKey, i_messageToProcess) ;
        }
        return messageProcessedIndicator ;
    }
    
    // The Boolean return value tells whether the message was consumed or not
    protected Boolean processMessage(MessageContainer i_messageToProcess) throws Exception
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.processMessage") ;
        // Empty implementation. To be implemented when the thread is used
        return false ;
    }
    
    protected void handlePingResponse(MessageContainer i_pingResponseMessageContainer) 
            throws Exception
    {
        // Empty implementation.
    }
    
    @Override
    protected InetSocketAddress getInetSocketAddressForConnection() throws Exception
    {
        this.logMessage("Entering SSRSocketP2PCommunicationManagerThread.getInetSocketAddressForConnection");
        this.logMessage("Using socket address parameters for p2p socket " 
                + "communication service instead of reading from parameters hashmap.");
        String serverIPAddress = SocketTunnellingBackendServer.SERVER_IP_ADDRESS ;
        Integer serverSocketNumber = SocketTunnellingBackendServer.SERVER_PORT;

        String oneLogMessage = "IP : " + serverIPAddress
                + " port : " + serverSocketNumber
                + " timeout : " + this.m_socketConnectionTimeoutInMilliseconds + " ms";
        this.logMessage(oneLogMessage, null);
        InetSocketAddress toReturn = new InetSocketAddress(serverIPAddress, serverSocketNumber);
        return toReturn;
    }
    
    private synchronized void maintainConnection() throws Exception
    {
        if (false == this.m_socketToBackend.isConnected()) 
        {
            this.logMessage("Connection to backend broken. Re-establishing connection.");
            this.registerPeerWithBackendForCommunication();
        }
        this.m_objectStreamForMessageFromBackend = new ObjectInputStream(this
                .m_socketToBackend.getInputStream());
    }
}
