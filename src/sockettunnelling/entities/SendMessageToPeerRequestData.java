/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.entities;

import java.io.Serializable;
import ssrcommon.entities.ISSRRequestData;

/**
 *
 * @author Siddhartha
 * Purpose : This class is the datastructure that is sent to the socket communication
 *           backend server. It wraps the actual data that is passed on to the client
 */
public class SendMessageToPeerRequestData implements ISSRRequestData, Serializable
{
    protected String m_destinationPeerId ;
    protected String m_sourcePeerId ;
    protected MessageContainer m_messageData ;

    public void setDestinationPeerId(String i_destinationPeerId) 
    {
        this.m_destinationPeerId = i_destinationPeerId;
    }

    public void setSourcePeerId(String i_sourcePeerId) 
    {
        this.m_sourcePeerId = i_sourcePeerId;
    }

    public void setMessageData(MessageContainer i_messageData) 
    {
        this.m_messageData = i_messageData;
    }

    public String getDestinationPeerId() 
    {
        return m_destinationPeerId;
    }

    public String getSourcePeerId() 
    {
        return m_sourcePeerId;
    }

    public MessageContainer getMessageData() 
    {
        return m_messageData;
    }
    
    @Override
    public Boolean vaidate() throws Exception 
    {
        return true ;
    }
}
