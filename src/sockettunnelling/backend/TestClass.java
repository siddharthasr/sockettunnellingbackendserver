/************************************************************************
 * This class can be used as a basic template for executing a basic client
 * server thing.
 ************************************************************************/
package sockettunnelling.backend;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import ssrcommon.threads.SSRBasicBackendServicesSocketServer;

public class TestClass 
{    
    public static void main(String[] args)
    {
        System.out.println("Starting socket tunnelling backend server.") ;
        try
        {
            SSRBasicBackendServicesSocketServer server = new SocketTunnellingBackendServer(
                    SocketTunnellingBackendServer.SERVER_PORT, 
                    SocketTunnellingBackendServer.CONTROL_PORT) ;
            server.start() ;
            
            // TODO : This check is not good. It will cause race condition. Fix this.
            //while (false == server.m_stopFlag) 
            while(Thread.State.TERMINATED != server.getState())
            {
                // sleep for 5 seconds before checking again.
                Thread.sleep(5000) ;
            }
            //System.out.println("Stop flag of backend server has been toggled to true. Exitting server.") ;
            System.out.println("Backend server thread terminated. Exitting server.") ;
            System.exit(0) ;
        }
        catch(Exception e)
        {
            System.out.println("Exception caught in main server : " + e.getMessage()) ;
            e.printStackTrace(System.out) ;
        }
    }
    /*public static void main(String[] args)
    {
        //AegisBackendServicesSocketServer server = new AegisBackendServicesSocketServer() ;
        //server.start() ;

        try
        {
            Socket clientSocket = new Socket() ;
            clientSocket.connect(new InetSocketAddress(new String("159.203.132.191"), 
                            AegisBackendServicesSocketServer.SERVER_PORT)) ;
            AegisBackendServicesSocketServerRequest request = 
                            new AegisBackendServicesSocketServerRequest() ;
            String queryToRun = new String("select t.* from tracking_session t") ;
            request.m_requestId = "1" ;
            request.m_serviceRequestCode =
                            AegisBackendServicesSocketServerRequest.RUN_CUSTOM_QUERY_REQUEST;
            request.m_requestDataSize = queryToRun.length() ;
            request.m_requestData = queryToRun ;

            OutputStream outputStreamToSend = clientSocket.getOutputStream() ;
            ObjectOutputStream objectOutputStreamToSend =
                            new ObjectOutputStream(outputStreamToSend) ;

            objectOutputStreamToSend.writeObject(request.m_requestId) ;
            objectOutputStreamToSend.writeObject(request.m_serviceRequestCode) ;
            objectOutputStreamToSend.writeObject(request.m_requestDataSize) ;
            objectOutputStreamToSend.writeObject(request.m_requestData) ;

            System.out.println("Request was sent.") ;

            AegisBackendServicesSocketServerResponse response = new AegisBackendServicesSocketServerResponse() ;

            InputStream responseInputStream = clientSocket.getInputStream() ;
            ObjectInputStream responseObjectInputStream = new ObjectInputStream(responseInputStream) ;

            response.m_responseId = (String)responseObjectInputStream.readObject() ;
            response.m_serverResponseCode = (String)responseObjectInputStream.readObject() ;
            response.m_responseDataSize = (String)responseObjectInputStream.readObject() ;
            response.m_responseData = (Object[][])responseObjectInputStream.readObject() ;

            System.out.println("Response was received.") ;

        }
        catch (Exception e)
        {
            System.out.println("Exception caught 1 : " + e.getMessage()) ;
        }
    }*/
}
