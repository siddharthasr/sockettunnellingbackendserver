/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.backend;

import java.io.Serializable;
import java.net.Socket;
import java.util.Vector;
import sockettunnelling.entities.MessageContainer;
import ssrcommon.SSRMutexProtectedOperation;
import ssrcommon.SSROrderedMutexGateKeeperForObject;
import ssrcommon.SSROrderedMutexProtectedHashMap;
import ssrcommon.threads.SSRBasicBackendServicesSocketServer;
import ssrcommon.threads.SSRBasicBackendServicesWorkerThread;
import ssrcommon.threads.SSRThreadForBackgroundWork;

/**
 *
 * @author LENOVO
 */
public class SocketTunnellingBackendServer extends SSRBasicBackendServicesSocketServer 
        implements Cloneable
{
    public static final String SERVER_IP_ADDRESS = "159.203.132.191" ;
    public static final Integer SERVER_PORT = 8100 ;
    public static final Integer CONTROL_PORT = 8110 ;
    
    protected SSROrderedMutexProtectedHashMap<String, RegisteredClientAddress> m_registeredClients ;
    protected SSROrderedMutexProtectedHashMap<String, SSROrderedMutexGateKeeperForObject<
            Vector<MessageContainer>>> m_messageQueue ;
    
    protected PostMasterThread m_postMasterThread ;
    
    public class RegisteredClientAddress
    {
        protected String m_clientId ;
        protected Socket m_clientSocket ;

        public void setClientId(String i_clientId) 
        {
            this.m_clientId = i_clientId;
        }

        public void setClientSocket(Socket i_clientSocket) 
        {
            this.m_clientSocket = i_clientSocket;
        }

        public String getClientId() 
        {
            return m_clientId;
        }

        public Socket getClientSocket() 
        {
            return m_clientSocket;
        }
    }
    
    public SocketTunnellingBackendServer(Integer i_serverPort, Integer i_serverControlPort) 
            throws Exception
    {
        super(i_serverPort, i_serverControlPort) ;
        this.m_registeredClients = new SSROrderedMutexProtectedHashMap<String, RegisteredClientAddress>() ;
        this.m_messageQueue = new SSROrderedMutexProtectedHashMap<String, 
                SSROrderedMutexGateKeeperForObject<Vector<MessageContainer>>>() ;
    }
    
    public SSROrderedMutexProtectedHashMap<String, RegisteredClientAddress> getRegisteredClientsHashMap() 
            throws Exception
    {
        return this.m_registeredClients ;
    }
    
    public SSROrderedMutexProtectedHashMap<String, SSROrderedMutexGateKeeperForObject<Vector<MessageContainer>>>
            getClientsMessageQueue() throws Exception
    {
        return this.m_messageQueue ;
    }
    
    public RegisteredClientAddress createRegisteredClientAddressInstance() throws Exception
    {
        return new RegisteredClientAddress() ;
    }
    
    public SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean> 
        getInsertMessageOperation() throws Exception
    {
        final MessageContainer a ;
        SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean> toReturn = 
                new SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean>()
                {
                    @Override
                    public void executeOperation(Vector<MessageContainer> i_objectForOperation) 
                            throws Exception 
                    {
                        MessageContainer containerToAdd = (MessageContainer)this.m_operationParameters[0] ;
                        i_objectForOperation.add(containerToAdd) ;
                    }
                } ;
        return toReturn ;
    }
        
    public SSRMutexProtectedOperation<Vector<MessageContainer>, MessageContainer> 
        getRemoveFirstMessageOperation() throws Exception
    {
        final MessageContainer a ;
        SSRMutexProtectedOperation<Vector<MessageContainer>, MessageContainer> toReturn = 
                new SSRMutexProtectedOperation<Vector<MessageContainer>, MessageContainer>()
                {
                    @Override
                    public void executeOperation(Vector<MessageContainer> i_objectForOperation) 
                            throws Exception 
                    {
                        this.m_operationResult = i_objectForOperation.firstElement() ;
                        i_objectForOperation.remove(0) ;
                    }
                } ;
        return toReturn ;
    }
    
    public SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean>
        getIsEmptyOperation() throws Exception 
    {
        SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean> toReturn
                = new SSRMutexProtectedOperation<Vector<MessageContainer>, Boolean>() 
                {
                    @Override
                    public void executeOperation(Vector<MessageContainer> i_objectForOperation)
                            throws Exception 
                    {
                        this.m_operationResult = i_objectForOperation.isEmpty() ;
                    }
        };
        return toReturn;
    }
        
    /*public SSRMutexProtectedOperation<Vector<MessageContainer>, String>
        getSynchObjectSingleVolley() throws Exception
    {
                SSRMutexProtectedOperation<Vector<MessageContainer>, String> toReturn
                = new SSRMutexProtectedOperation<Vector<MessageContainer>, String>() 
                {
                    @Override
                    public void executeOperation(Vector<MessageContainer> i_objectForOperation)
                            throws Exception 
                    {
                        this.m_operationResult = i_objectForOperation.isEmpty() ;
                    }
        };
        return toReturn;
    }*/
        
    public void wakeupPostMaster() throws Exception
    {
        this.m_postMasterThread.issueWakeupRequest() ;
    }
        
    protected void performAuxiliaryHandling() throws Exception
    {
        try
        {
            this.m_postMasterThread = new PostMasterThread(this) ;
            this.m_postMasterThread.start() ;
        }
        catch(Exception e)
        {
            this.handleThreadFlowException("Exception in SocketTunnelllingBackendServer.performAuxiliaryHandling :\n", e);
        }
    }
    
    protected SSRBasicBackendServicesWorkerThread getOneWorkerThread(Socket i_clientSocket)
            throws Exception
    {
        return new SocketTunnellingBackendWorkerThread(i_clientSocket, this) ;
    }
}
