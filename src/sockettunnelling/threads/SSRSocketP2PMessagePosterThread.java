/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockettunnelling.threads;

import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import sockettunnelling.backend.SocketTunnellingBackendServer;
import sockettunnelling.backend.SocketTunnellingBackendWorkerThread;
import sockettunnelling.entities.SendMessageToPeerRequestData;
import ssrcommon.threads.ISSRBackendRequest;
import ssrcommon.threads.SSRBasicBackendRequest;
import ssrcommon.threads.SSRBasicBackendResponse;
import ssrcommon.threads.SSRBasicBackendServicesThread;

/**
 *
 * @author LENOVO
 */
public class SSRSocketP2PMessagePosterThread extends SSRBasicBackendServicesThread
{
    protected SendMessageToPeerRequestData m_messageRequestDataToSend ;
    
    public SSRSocketP2PMessagePosterThread(SendMessageToPeerRequestData i_messageRequestData) 
            throws Exception
    {
        this(new HashMap<Object, Object>(), i_messageRequestData);
    }
    
    public SSRSocketP2PMessagePosterThread(HashMap<Object, Object> i_parametersHashMap, 
            SendMessageToPeerRequestData i_messageRequestData) throws Exception
    {
        this(i_parametersHashMap, SSRBasicBackendServicesThread
                .DEFAULT_SOCKET_CONNECTION_TIMEOUT_DURATION, i_messageRequestData);
    }
        
    public SSRSocketP2PMessagePosterThread(HashMap<Object, Object> i_parametersHashMap,
            Integer i_socketTimeoutToSet, SendMessageToPeerRequestData i_messageRequestData) 
            throws Exception
    {
        super(null, null, i_parametersHashMap, i_socketTimeoutToSet);
        this.m_messageRequestDataToSend = i_messageRequestData ;
    }
    
    @Override
    public void runMainLogic() throws Exception
    {
        this.logMessage("Entering SSRSocketP2PMessagePosterThread.runMainLogic") ;
        // Create request object
        SSRBasicBackendRequest requestForPostingMessage = new SSRBasicBackendRequest();
        this.m_requestToBackend = requestForPostingMessage ;
        requestForPostingMessage.m_requestId = "1";
        requestForPostingMessage.m_serviceRequestCode = SocketTunnellingBackendWorkerThread.SEND_MESSAGE_TO_PEER;
        requestForPostingMessage.m_requestDataSize = new Integer(1);
        requestForPostingMessage.m_requestData = this.m_messageRequestDataToSend ;

        // Create response object
        SSRBasicBackendResponse responseFromBackend = new SSRBasicBackendResponse() ;
        this.m_responseFromBackend = responseFromBackend ;
        
        // Handle connection establishment
        this.handleConnectionEstablishment();
        
        // Send request
        requestForPostingMessage.writeRequestToOutputStream(this.m_socketToBackend
                .getOutputStream());
        
        // Read response
        responseFromBackend.readResponseFromInputStream(this.m_socketToBackend
                .getInputStream());
        if(false == responseFromBackend.m_serverResponseCode.equals(SSRBasicBackendResponse
                .RESPONSE_OK))
        {
            this.logMessage("Server response indicates failure :\n" 
                    + responseFromBackend.toString()) ;
            this.m_status = SSRBasicBackendServicesThread.ThreadFlowStatus.FINISH_FAILURE ;
        }
    }
    
    @Override
    protected InetSocketAddress getInetSocketAddressForConnection() throws Exception
    {
        this.logMessage("Entering SSRSocketP2PMessagePosterThread.getInetSocketAddressForConnection");
        this.logMessage("Using socket address parameters for p2p socket " 
                + "communication service instead of reading from parameters hashmap.");
        String serverIPAddress = SocketTunnellingBackendServer.SERVER_IP_ADDRESS ;
        Integer serverSocketNumber = SocketTunnellingBackendServer.SERVER_PORT;

        String oneLogMessage = "IP : " + serverIPAddress
                + " port : " + serverSocketNumber
                + " timeout : " + this.m_socketConnectionTimeoutInMilliseconds + " ms";
        this.logMessage(oneLogMessage, null);
        InetSocketAddress toReturn = new InetSocketAddress(serverIPAddress, serverSocketNumber);
        return toReturn;
    }
}
